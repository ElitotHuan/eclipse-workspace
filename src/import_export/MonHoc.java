package import_export;

public class MonHoc {
	private String maMH, tenMH;
	private double diem;

	public MonHoc() {
		super();

	}

	public MonHoc(String maMH, String tenMH, double diem) {
		super();
		this.maMH = maMH;
		this.tenMH = tenMH;
		this.diem = diem;
	}

	@Override
	public String toString() {
		return "MonHoc [maMH=" + maMH + ", tenMH=" + tenMH + ", diem=" + diem + "]";
	}

	public String getMaMH() {
		return maMH;
	}

	public String getTenMH() {
		return tenMH;
	}

	public double getDiem() {
		return diem;
	}

	public void setMaMH(String maMH) {
		this.maMH = maMH;
	}

	public void setTenMH(String tenMH) {
		this.tenMH = tenMH;
	}

	public void setDiem(double diem) {
		this.diem = diem;
	}

}
