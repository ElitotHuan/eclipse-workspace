package import_export;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Import_Export {

	public static void export(String path, ArrayList<SinhVien> list, String charset, String demilited)
			throws IOException {
		File s = new File(path);

		OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(s), charset);
		PrintWriter pw = new PrintWriter(osw);
		for (SinhVien sinhVien : list) {
			pw.println(sinhVien.line(demilited));
		}

		pw.close();

	}

	public static ArrayList<SinhVien> importObject(String path, String charset, String demilited) throws IOException {
		ArrayList<SinhVien> re = new ArrayList<>();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)), charset));
		String line = "";

		while ((line = br.readLine()) != null) {
			String[] arrayLine = line.split(demilited);
			SinhVien sv = new SinhVien();
			if (arrayLine.length > 1) {
				sv.setMasV(arrayLine[0]);
				sv.setHoTen(arrayLine[1]);
				
			}

			if (arrayLine.length > 3) {
				MonHoc mh = new MonHoc();
				for (int i = 2; i < arrayLine.length; i++) {
					if (i % 2 == 0) {
						mh.setTenMH(arrayLine[i]);
					} else {
						mh.setDiem(Double.parseDouble(arrayLine[i]));
						sv.addMH(mh);
						mh = new MonHoc();
					}
				}
			}

			re.add(sv);

		}

		return re;

	}

	public static void main(String[] args) throws IOException {
		String s = "D:\\test\\export.txt";

		SinhVien sv1 = new SinhVien("1", "Nguyễn Văn Nam", 20);
		sv1.addMH(new MonHoc("23232", "Lập trình mạng", 9.0));
		sv1.addMH(new MonHoc("34343", "Lập trình Web", 5.0));
		sv1.addMH(new MonHoc("34323", "Trí tuệ nhân tạo", 7.0));

		SinhVien sv2 = new SinhVien("2", "Trần Như", 20);
		sv2.addMH(new MonHoc("23232", "Lập trình mạng", 10.0));
		sv2.addMH(new MonHoc("34343", "Lập trình Web", 8.0));
		sv2.addMH(new MonHoc("34323", "Trí tuệ nhân tạo", 7.0));

		SinhVien sv3 = new SinhVien("3", "Lê Văn Tâm", 20);
		sv3.addMH(new MonHoc("23232", "Lập trình mạng", 5.0));
		sv3.addMH(new MonHoc("34343", "Lập trình Web", 8.0));
		sv3.addMH(new MonHoc("34323", "Trí tuệ nhân tạo", 7.0));

		ArrayList<SinhVien> list = new ArrayList<>();
		list.add(sv1);
		list.add(sv2);
		list.add(sv3);

//		export(s, list, "UTF-8", "\t");
		ArrayList<SinhVien> im = importObject(s, "UTF-8", "\t");
		for (SinhVien sinhVien : im) {
			System.out.println(sinhVien);
		}

	}
}
