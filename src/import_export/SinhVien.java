package import_export;

import java.io.Serializable;
import java.util.ArrayList;

public class SinhVien implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String masV, hoTen;
	private int tuoi;
	ArrayList<MonHoc> dsmh;

	
	public SinhVien() {
		dsmh = new ArrayList<MonHoc>();
	}
	
	public SinhVien(String masV, String hoTen, int tuoi) {
		this.masV = masV;
		this.hoTen = hoTen;
		this.tuoi = tuoi;
		this.dsmh = new ArrayList<MonHoc>();

	}

	public void addMH(MonHoc e) {
		if (!dsmh.contains(e)) {
			dsmh.add(e);
		}

	}
	
	
	
	public String toString() {
		String result = "";
		result = "\t" + masV + "\t" + hoTen;
		for (MonHoc monHoc : dsmh) {
			result += "\t" + monHoc.getTenMH() + "\t" + monHoc.getDiem();
		}
		result += "\n";
		return result;
	}
	
	public String line(String demilited) {
		String line ="";
		line += masV + demilited  + hoTen;
		for (MonHoc monHoc : dsmh) {
			line += demilited + monHoc.getTenMH() + demilited + monHoc.getDiem();
		}
		return line;
	}

	public String getMasV() {
		return masV;
	}

	public String getHoTen() {
		return hoTen;
	}

	public int getTuoi() {
		return tuoi;
	}

	public void setMasV(String masV) {
		this.masV = masV;
	}

	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}

	public void setTuoi(int tuoi) {
		this.tuoi = tuoi;
	}

	public void setDsmh(ArrayList<MonHoc> dsmh) {
		this.dsmh = dsmh;
	}
	
	
	

}
