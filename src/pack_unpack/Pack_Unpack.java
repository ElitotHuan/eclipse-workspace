package pack_unpack;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Pack_Unpack {

    public static void pack(String pathFolder, String pathPackFile) throws IOException {
        File s = new File(pathFolder);
        if (!s.exists()) {
            return;
        } else if (s.isFile()) {
            return;
        } else {

            File[] lsFiles = s.listFiles();
            RandomAccessFile raf = new RandomAccessFile(pathPackFile, "rw");
            raf.writeInt(lsFiles.length);
            for (File file : lsFiles) {
                raf.writeUTF(file.getName());
                raf.writeLong(file.length());
                BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
                int data;
                while ((data = bis.read()) != -1) {
                    raf.write(data);
                }

                bis.close();
            }

            raf.close();

        }

    }

    public static void unpack(String pathFolder, String pathSourceFile, String name) throws IOException {
        RandomAccessFile raf = new RandomAccessFile(pathSourceFile, "rw");
        int count = raf.readInt();
        for (int i = 0; i < count; i++) {
            String fname = raf.readUTF();
            long size = raf.readLong();
            long pos = raf.getFilePointer();
            if (fname.equals(name)) {
                BufferedOutputStream bos = new BufferedOutputStream(
                        new FileOutputStream(pathFolder + "\\unpack-" + name));
                byte[] buffer = new byte[1024 * 1000];
                int data;
                int temp = 0;
                int remain = (int) (size % buffer.length);
                int count1 = (int) (size / buffer.length);
                int count2 = 0;
                while ((data = raf.read(buffer)) != -1) {
                    temp += data;
                    if (count1 == count2) {
                        bos.write(buffer, 0, remain);
                    } else {
                        bos.write(buffer, 0, data);
                    }

                    count2++;

                    if (temp > size) {
                        break;
                    }

                }

            } else {
                raf.seek(pos + size);
            }
        }

        raf.close();
    }

    public static void main(String[] args) throws IOException {
//		String pack = "D:\\Test\\ltm";
        String archive = "D:\\Test\\KQ\\pack.ltm";
        String name = "IntroToNetProgramming.pptx";
//		pack(pack, archive);

        unpack("D:\\Test", archive, name);
    }
}
