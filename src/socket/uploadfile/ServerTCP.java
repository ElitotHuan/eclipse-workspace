/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket.uploadfile;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Acer
 */
public class ServerTCP {
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(9999);
        System.out.println("Waiting for connection");
        while (true) {
            Socket socket = server.accept();
            Thread t = new Process(socket);
            t.start();
        }

    }

}
