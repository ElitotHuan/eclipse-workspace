package socket.uploadfile;

import java.io.*;
import java.net.Socket;
import java.util.StringTokenizer;

public class Process extends Thread {
    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;
    public String serverDir = "D:\\Test\\server";
    public String clientDir = "D:\\Test\\client";

    public Process(Socket socket) throws IOException {
        this.socket = socket;
        dis = new DataInputStream(this.socket.getInputStream());
        dos = new DataOutputStream(this.socket.getOutputStream());
    }

    @Override
    public void run() {
        try {
            dos.writeUTF("Welcome Cleint");
            dos.flush();
            while (true) {
                try {
                    String command = dis.readUTF();
                    if ("QUIT".equalsIgnoreCase(command)) {
                        dos.writeUTF("Goodbye Client");
                        dis.close();
                        dos.close();
                        break;
                    } else {
                        StringTokenizer tokenizer = new StringTokenizer(command, " ");
                        String request = tokenizer.nextToken();

                        switch (request) {
                            case "copy" -> {
                                String source = tokenizer.nextToken();
                                String dest = tokenizer.nextToken();
                                if (tokenizer.hasMoreTokens()) {
                                    dos.writeUTF("-1");
                                    dos.flush();
                                    break;
                                }
                                dos.writeUTF(clientDir);
                                dos.flush();
                                long fileSize = dis.readLong();
                                copyFile(dest, fileSize);
                                dos.writeUTF("Upload to server complete");
                                break;
                            }

                            case "get" -> {

                            }


                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("Loi");
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public void copyFile(String d, long fileSize) throws IOException {
        File dest = new File(serverDir + "\\" + d);
        OutputStream os = new BufferedOutputStream(new FileOutputStream(dest));
        dos.writeInt(0);
        dos.flush();
        for (int i = 0; i < fileSize; i++) {
            os.write(dis.read());
            os.flush();
        }
        os.close();
    }


}

