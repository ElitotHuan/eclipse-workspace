/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket.uploadfile;

import java.io.*;
import java.net.Socket;
import java.util.StringTokenizer;

/**
 * @author Acer
 */
public class ClientTCP {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 9999);
        DataInputStream dis = new DataInputStream(socket.getInputStream());
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

        String hello = dis.readUTF();
        System.out.println(hello);
        while (true) {
            BufferedReader bread = new BufferedReader(new InputStreamReader(System.in));
            String command = bread.readLine();
            dos.writeUTF(command);
            dos.flush();

            StringTokenizer tokenizer = new StringTokenizer(command, " ");
            String key = tokenizer.nextToken();
            switch (key) {
                case "copy": {
                    String pathDirectoryUploadAtClient = dis.readUTF();

                    if (pathDirectoryUploadAtClient.equals("-1")) {
                        System.out.println("Too many param");
                        break;
                    }

                    String source = tokenizer.nextToken();
                    File s = new File(pathDirectoryUploadAtClient + "\\" + source);

                    if (!s.exists()) {
                        System.out.println("Source file doesn't exit");
                        break;
                    } else {
                        InputStream is = new BufferedInputStream(new FileInputStream(s));
                        dos.writeLong(s.length());
                        dos.flush();
                        int data;
                        while ((data = is.read()) != -1) {
                            dos.write(data);
                            dos.flush();
                        }
                        is.close();
                    }
                    break;

                }

                default: {
                    break;
                }
            }

            String reply = dis.readUTF();
            System.out.println(reply);

            if (reply.equalsIgnoreCase("Goodbye Client!")) {
                dis.close();
                dos.close();
                bread.close();
                break;
            }


        }

    }
}
