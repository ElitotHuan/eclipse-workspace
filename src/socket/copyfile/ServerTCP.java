package socket.copyfile;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerTCP {

    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(34567);
        File source = new File("D:\\Test\\test2.docx");
        System.out.println("Waiting for client");
        while (true) {
            Socket socket = server.accept();
            InputStream is = new BufferedInputStream(new FileInputStream(source));
            OutputStream os = new BufferedOutputStream(socket.getOutputStream());
            byte[] bytes = new byte[1024];
            int data;
            while ((data = is.read(bytes)) != -1) {
                os.write(bytes, 0, data);
                os.flush();

            }

          
            os.close();
            is.close();

        }

    }
}
