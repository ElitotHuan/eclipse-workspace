package socket.copyfile;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ClientTCP {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 34567);
        System.out.println("Connect complete");
        File dest = new File("D:\\Test\\test2-copy.docx");
        int size = (int) dest.length();
        byte[] bytes = new byte[1024];
        InputStream is = new BufferedInputStream(socket.getInputStream());
        OutputStream os = new BufferedOutputStream(new FileOutputStream(dest));
        int data;
        while ((data = is.read(bytes)) != -1) {
            os.write(bytes, 0, data);
            os.flush();           
        }
        os.close();
        is.close();
        socket.close();
        
    }
}
