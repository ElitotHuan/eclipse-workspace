package socket.managestudent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TruyVanSQL {
    Connection connect = new Connect().getConnection();

    public TruyVanSQL() throws Exception {

    }

    public static void main(String[] args) throws Exception {
        TruyVanSQL tr = new TruyVanSQL();
//        Student student = new Student(18130089, "Vo Doan Minh Huan", "user1", "123456", 21);
//        Boolean updated = tr.delete(1813123);
//        System.out.println(updated);

        ArrayList<Student> list = tr.getStudents();
        for (Student  s1 : list){
            System.out.println(s1);
        }

//        String data = "18130089\tUser1";
//        String[] array = data.split("\t");
//        System.out.println(array[1]);
    }

    public ArrayList<Student> getStudents() throws SQLException {
        ArrayList<Student> results = new ArrayList<Student>();
        String sql = "SELECT * FROM student";
        PreparedStatement stmt = connect.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            Student student = new Student(rs.getInt("ID"), rs.getString("Name"), rs.getString("Username"), rs.getString("Password"), rs.getInt("Age"));
            results.add(student);
        }
        stmt.close();

        String sql2 = "SELECT * FROM Subject";
        PreparedStatement stmt2 = connect.prepareStatement(sql2);
        ResultSet rs2 = stmt2.executeQuery();
        while (rs2.next()) {
            for (Student student : results) {
                if (student.getId() == rs2.getInt("ID")) {
                    Subject subject = new Subject(rs2.getString("Subject"), rs2.getDouble("Score"));
                    student.addSubject(subject);
                }
            }

        }
        stmt2.close();

        return results;
    }

    public boolean insert(Student student) {
        String insert = "INSERT INTO Student(ID , Name , Age , Username , Password) VALUES (?,?,?,?,?)";
        try {
            PreparedStatement stmt = connect.prepareStatement(insert);
            stmt.setInt(1, student.getId());
            stmt.setString(2, student.getName());
            stmt.setInt(3, student.getAge());
            stmt.setString(4, student.getUsername());
            stmt.setString(5, student.getPassword());
            stmt.executeUpdate();

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
            return false;
        }

        return true;
    }

    public boolean update(Student student) {
        String update = "UPDATE Student SET Student.ID = ? , Student.Name = ? , Student.Age = ? , Student.Username = ? , Student.Password = ? WHERE Student.ID = ? ";
        try {
            PreparedStatement stmt = connect.prepareStatement(update);
            stmt.setInt(1, student.getId());
            stmt.setString(2, student.getName());
            stmt.setInt(3, student.getAge());
            stmt.setString(4, student.getUsername());
            stmt.setString(5, student.getPassword());
            stmt.setInt(6, student.getId());
            stmt.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }

        return true;

    }

    public boolean delete(int ID) {
        String delete = "DELETE FROM Student WHERE Student.ID = ?";
        try {
            PreparedStatement stmt = connect.prepareStatement(delete);
            stmt.setInt(1, ID);
            stmt.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean login(String username, String password) throws SQLException {
        String sql = "SELECT * FROM STUDENT WHERE Username = ? AND  Password = ?";
        PreparedStatement stmt = connect.prepareStatement(sql);
        stmt.setString(1, username);
        stmt.setString(2, password);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            return true;

        }

        return false;
    }
}
