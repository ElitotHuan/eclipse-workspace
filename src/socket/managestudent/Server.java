package socket.managestudent;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(12345);
        System.out.println("Waiting for connection");
        while (true){
            Socket socket = serverSocket.accept();
            Thread t = new Process(socket);
            t.start();
        }
    }
}
