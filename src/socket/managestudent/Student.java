package socket.managestudent;

import java.util.ArrayList;

public class Student {
    private int id;
    private String name, username, password;
    private int age;
    private ArrayList<Subject> subjects = new ArrayList<>();

    public Student(int id, String name, String username, String password, int age) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", subjects=" + subjects +
                '}';
    }

    public void addSubject(Subject subject) {
        subjects.add(subject);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getAge() {
        return age;
    }
}
