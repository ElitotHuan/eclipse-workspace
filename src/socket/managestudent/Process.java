package socket.managestudent;

import java.io.*;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Process extends Thread {
    private final BufferedReader in;
    private final PrintWriter out;
    private ArrayList<Student> studentList;
    private String userName, passWord;
    private Boolean success = false;
    private final TruyVanSQL truyVanSQL;

    public Process(Socket socket) throws Exception {
        this.truyVanSQL = new TruyVanSQL();
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    @Override
    public void run() {
        try {
            out.println("Hello Student");
            out.flush();
            while (true) {
                String command = in.readLine();
                if (command.equalsIgnoreCase("Quit")) {
                    out.println("Goodbye");
                    out.flush();
                    in.close();
                    out.close();
                    break;
                } else {
                    StringTokenizer tokenizer = new StringTokenizer(command);
                    String request = tokenizer.nextToken();
                    if (request.equals("Username:")) {
                        userName = tokenizer.nextToken();
                        out.println("Nhập lệnh kế tiếp");
                        out.flush();
                    } else {
                        if (request.equals("Password:")) {
                            passWord = tokenizer.nextToken();
                            boolean check = checkLogin(userName, passWord);
                            if (check) {
                                success = true;
                                out.println("Đăng nhập thành công");
                            } else {
                                out.println("Đăng nhập thất bại");
                            }
                            out.flush();
                        } else {
                            if (success) {
                                tokenizer = new StringTokenizer(command, "\t");
                                if (tokenizer.countTokens() != 2) {
                                    out.println("Command wrong");
                                    out.flush();
                                }
                                String key = tokenizer.nextToken();
                                String enter = tokenizer.nextToken();
                                switch (key) {

                                    case "Show" -> {
                                        if (enter.equalsIgnoreCase("StudentList")){
                                            String result = getStudentList().toString();
                                            out.println(result);
                                            out.flush();
                                        }
                                    }

                                    case "findByName" -> {
                                        if (findByName(enter).size() != 0) {
                                            String result = findByName(enter).toString();
                                            out.println(result);
                                        } else {
                                            out.println("No student found");
                                        }
                                        out.flush();
                                    }

                                    case "Insert" -> {
                                        String[] data = enter.split("\t");
                                        Student student = new Student(Integer.parseInt(data[0]), data[1], data[2], data[3], Integer.parseInt(data[4]));
                                        boolean insert = insertStudent(student);
                                        if (insert) {
                                            out.println("Thêm thành công");
                                        } else {
                                            out.println("Dữ liệu vào không chính xác");
                                        }
                                        out.flush();

                                    }

                                    case "Update" -> {
                                        String[] data = enter.split("\t");
                                        Student student = new Student(Integer.parseInt(data[0]), data[1], data[2], data[3], Integer.parseInt(data[4]));
                                        boolean update = updateStudent(student);
                                        if (update) {
                                            out.println("Cập nhật thành công");

                                        } else {
                                            out.println("Dữ liệu không chính xác");
                                        }

                                        out.flush();

                                    }

                                    case "Delete" -> {
                                        boolean delete = deleteStudent(Integer.parseInt(enter));
                                        if (delete){
                                            out.println("Xóa thành công");

                                        } else {
                                            out.println("Dữ liệu không chính xác");
                                        }

                                        out.flush();
                                    }

                                    default -> {
                                    }
                                }
                            } else {
                                out.println("Bạn phải đăng nhập trước");
                                out.flush();
                            }
                        }
                    }


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean checkLogin(String username, String password) throws SQLException {
        return truyVanSQL.login(username, password);
    }

    public ArrayList<Student> getStudentList() throws SQLException {
        return truyVanSQL.getStudents();
    }

    public boolean insertStudent(Student student) {
        return truyVanSQL.insert(student);
    }

    public boolean updateStudent(Student student) {
        return truyVanSQL.update(student);
    }

    public boolean deleteStudent(int id) {
        return truyVanSQL.delete(id);
    }

    public ArrayList<Student> findByName(String name) throws SQLException {
        ArrayList<Student> result = new ArrayList<>();
        for (Student student : getStudentList()) {
            if (student.getName().equalsIgnoreCase(name)) {
                result.add(student);
            }
        }

        return result;
    }


}
