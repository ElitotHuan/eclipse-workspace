/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket.mathTCP;

import java.io.*;
import java.net.Socket;
import java.util.*;

/**
 * @author Acer
 */
public class Server_Process extends Thread {

    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;

    public Server_Process(Socket socket) {

        try {
            this.socket = socket;
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        try {
            dos.writeUTF("Hello Client");
            dos.flush();
            while (true) {
                String command = dis.readUTF();
                if (command.equalsIgnoreCase("Quit")) {
                    dos.writeUTF("Goodbye Client!");
                    dis.close();
                    dos.close();
                    break;
                } else {
                    String operator = findOperator(command);
                    if (operator != null) {

                        double soA = 0;
                        double soB = 0;
                        double result = 0;
                        StringTokenizer token = new StringTokenizer(command, operator);

                        try {
                            soA = Double.parseDouble(token.nextToken());
                            soB = Double.parseDouble(token.nextToken());
                            switch (operator) {
                                case "+" -> {
                                    result = soA + soB;
                                    dos.writeUTF(soA + operator + soB + " = " + result);
                                    dos.flush();
                                }
                                case "-" -> {
                                    result = soA - soB;
                                    dos.writeUTF(soA + operator + soB + " = " + result);
                                    dos.flush();
                                }
                                case "*" -> {
                                    result = soA * soB;
                                    dos.writeUTF(soA + operator + soB + " = " + result);
                                    dos.flush();
                                }
                                case "/" -> {
                                    if (soB == 0){
                                        dos.writeUTF("Cant divide by 0");
                                    }else {
                                        result = soA / soB;
                                        dos.writeUTF(soA + operator + soB + " = " + result);
                                        dos.flush();
                                    }
                                }
                                default -> {
                                    break;
                                }
                            }
                        } catch (NumberFormatException e) {
                            dos.writeUTF("Operator wrong!!");
                            dos.flush();
                        }

                    } else {
                        dos.writeUTF("Input command wrong!!");
                        dos.flush();
                    }

                }

            }

        } catch (IOException e) {
            // TODO: handle exception
            e.printStackTrace();

        }

    }

    public static String findOperator(String command) {
        if (new StringTokenizer(command, "+").countTokens() == 2) {
            return "+";
        }

        if (new StringTokenizer(command, "-").countTokens() == 2) {
            return "-";
        }

        if (new StringTokenizer(command, "*").countTokens() == 2) {
            return "*";
        }

        if (new StringTokenizer(command, "/").countTokens() == 2) {
            return "/";
        }

        return null;
    }
}
