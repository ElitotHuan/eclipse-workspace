package socket.mathTCP;

import java.io.*;
import java.net.Socket;

/**
 * @author Acer
 */
public class Client {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 12345);
        System.out.println("Connect Complete");

        DataInputStream readFromServer = new DataInputStream(socket.getInputStream());
        DataOutputStream writeToServer = new DataOutputStream(socket.getOutputStream());


        String hello = readFromServer.readUTF();
        System.out.println(hello);
        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String command = br.readLine();
            writeToServer.writeUTF(command);
            writeToServer.flush();
            String reply = readFromServer.readUTF();
            System.out.println(reply);

            if (reply.equalsIgnoreCase("Goodbye Client!")) {
                readFromServer.close();
                writeToServer.close();
                br.close();
                break;
            }
        }


    }
}
