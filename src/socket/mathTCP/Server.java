
package socket.mathTCP;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Acer
 */
public class Server {

    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(12345);
        System.out.println("Waiting for connection");

        while (true) {
            Socket s = server.accept();
            Thread t = new Server_Process(s);
            t.start();
            System.out.println(s.getInetAddress() + "connected");
        }

    }
}
