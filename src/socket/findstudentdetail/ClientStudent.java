package socket.findstudentdetail;

import java.io.*;
import java.net.Socket;

public class ClientStudent {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 8888);
        System.out.println("Connected");
        BufferedReader dis = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintWriter dos = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

        String hello = dis.readLine();
        System.out.println(hello);

        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String command = br.readLine();
            dos.println(command);
            dos.flush();

            String reply = dis.readLine();

            System.out.println(reply);

            if (reply.equalsIgnoreCase("Goodbye")) {
                dis.close();
                dos.close();
                br.close();
                break;
            }
        }

    }
}
