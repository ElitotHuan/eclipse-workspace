package socket.findstudentdetail;

import java.io.*;
import java.net.Socket;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Process extends Thread {
    private Socket socket;
    private BufferedReader dis;
    private PrintWriter dos;
    private Connect connect = new Connect();
    private ArrayList<Student> list;

    public Process() {

    }

    public Process(Socket socket) throws IOException {
        this.socket = socket;
        dis = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        dos = new PrintWriter(new OutputStreamWriter(this.socket.getOutputStream()), true);
    }

    @Override
    public void run() {
        try {
            dos.println("Welcome");
            dos.flush();
            while (true) {
                String command = dis.readLine();
                if (command.equalsIgnoreCase("Quit")) {
                    dos.println("Goodbye");
                    dis.close();
                    dos.close();
                    break;
                } else {
                    StringTokenizer tokenizer = new StringTokenizer(command, "\t");
                    if (tokenizer.countTokens() != 2) {
                        dos.println("Command wrong");
                        dos.flush();
                    }
                    String key = tokenizer.nextToken();
                    String request = tokenizer.nextToken();
                    switch (key) {
                        case "findById" -> {

                            if (findById(request).size() != 0) {
                                String result = findById(request).toString();
                                dos.println(result);
                            } else {
                                dos.println("No student found");
                            }
                            dos.flush();
                            break;


                        }

                        case "findByName" -> {

                            if (findByName(request).size() != 0) {
                                String result = findByName(request).toString();
                                dos.println(result);
                            } else {
                                dos.println("No student found");
                            }
                            dos.flush();
                            break;


                        }

                        case "findByAge" -> {

                            int num = Integer.parseInt(request);
                            if (findByAge(num).size() != 0) {
                                String result = findByAge(num).toString();
                                dos.println(result);
                            } else {
                                dos.println("No student found");
                            }
                            dos.flush();

                            break;

                        }

                        case "findByScore" -> {

//                            double score = Double.parseDouble(request);
//                            if (findByScore(score).size() != 0) {
//                                String result = findByScore(score).toString();
//                                dos.println(result);
//                            } else {
//                                dos.println("No Student found");
//                            }
//                            dos.flush();

                            break;


                        }

                        default -> {
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Student> getList() throws Exception {
        list = new ArrayList<>();
//        String sql = "Select * from student";
//        PreparedStatement prempState = connect.getConnection().prepareStatement(sql);
//        ResultSet rs = prempState.executeQuery();
//        while (rs.next()){
//            Student student = new Student(rs.getString("Name"),rs.getInt("Age") , rs.getDouble("Score"));
//            list.add(student);
//        }

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("D:\\Test\\data.txt"), "UTF-8"));
        String line = "";
        while ((line = br.readLine()) != null) {
            String[] array = line.split("\t");
            String mssv = array[0];
            String name = array[1];
            int age = Integer.parseInt(array[2]);
            Student student = new Student(mssv, name, age);
            list.add(student);
        }

        BufferedReader readScores = new BufferedReader(new InputStreamReader(new FileInputStream("D:\\Test\\score.txt"), "UTF-8"));
        String line1 = "";
        while ((line1 = readScores.readLine()) != null) {
            String[] array = line1.split("\t");
            for (int i = 0; i < list.size(); i++) {
                if (array[0].equalsIgnoreCase(list.get(i).getId())) {
                    for (int j = 1; j < array.length; j++) {
                        list.get(i).addScore(Double.parseDouble(array[j]));
                    }
                }
            }
        }

        return list;
    }

    public ArrayList<Student> findById(String id) throws Exception {
        ArrayList<Student> result = new ArrayList<>();
        for (Student s : getList()) {
            if (s.getId().equalsIgnoreCase(id)) {
                result.add(s);
            }
        }
        return result;
    }

    public ArrayList<Student> findByName(String name) throws Exception {
        ArrayList<Student> result = new ArrayList<>();
        for (Student s : getList()) {
            if (s.getName().contains(name)) {
                result.add(s);
            }
        }
        return result;
    }

    public ArrayList<Student> findByAge(int age) throws Exception {
        ArrayList<Student> result = new ArrayList<>();
        for (Student s : getList()) {
            if (s.getAge() == age) {
                result.add(s);
            }
        }

        return result;
    }

    public ArrayList<Student> findByScore(double score) throws Exception {
        ArrayList<Student> result = new ArrayList<>();
        for (Student s : getList()) {
            for (int i = 0; i < s.getScores().size(); i++) {
                if (s.getScores().get(i) == score) {
                    result.add(s);
                }
            }
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        Process p = new Process();
        List<Student> t = p.findByScore(8);
        System.out.println(t.toString());
    }


}
