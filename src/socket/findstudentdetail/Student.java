package socket.findstudentdetail;


import java.util.ArrayList;
import java.util.List;

public class Student {
    private String id, name;
    private int age;
    private ArrayList<Double> scores;

    public Student(String id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.scores = new ArrayList<>();
    }

    @Override
    public String toString() {
        return
                "MSSV:" + id + "\t" +
                        "Tên:" + name + "\t" +
                        "Tuổi:" + age + "\t" +
                        "Điểm" + scores.toString();
    }

    public void addScore(Double score) {
        scores.add(score);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public ArrayList<Double> getScores() {
        return scores;
    }

    public void setScores(ArrayList<Double> scores) {
        this.scores = scores;
    }
}

