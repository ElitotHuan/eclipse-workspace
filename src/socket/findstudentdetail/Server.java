package socket.findstudentdetail;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8888);
        System.out.println("Waiting client connection");
        while (true){
            Socket socket = serverSocket.accept();
            Thread t = new Process(socket);
            t.start();
        }
    }
}
