package File;

import java.io.File;

public class Bai5 {
	public static void findAll(String path, String ext) {
		File dir = new File(path);

		File[] fList = dir.listFiles();

		for (int i = 0; i < fList.length; i++) {
			if (fList[i].isFile()) {
				if (fList[i].getName().endsWith(ext)) {
					System.out.println(fList[i].getPath());
				}

			} else {
				findAll(fList[i].getAbsolutePath(), ext);
			}
		}
	}

	public static void findAlls(String path, String[] exts) {
		for (int i = 0; i < exts.length; i++) {
			findAll(path, exts[i]);
		}
	}
}
