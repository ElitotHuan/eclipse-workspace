package File;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class Bai7 {

	public static void copy(String sDir, String dDir, String exts) throws IOException {
		File sPath = new File(sDir);
		
		File dPath = new File(dDir);

		File[] sListFile = sPath.listFiles();
		for (int i = 0; i < sListFile.length; i++) {

			if (sListFile[i].isFile() && sListFile[i].getName().endsWith(exts)) {
				Files.copy(sListFile[i].toPath(), new File(dPath + "\\" + sListFile[i].getName()).toPath(),
						StandardCopyOption.REPLACE_EXISTING);
			}
		}

	}

	public static void copyAll(String sDir, String dDir, String[] exts) throws IOException {
		for (int i = 0; i < exts.length; i++) {
			copy(sDir, dDir, exts[i]);
		}
	}

	public static void main(String[] args) throws IOException {
		String[] exts = { ".docx", ".txt" };
		copyAll("D:\\Test", "D:\\Test\\f2", exts);
	}
}
