package File;

import java.io.File;

public class Bai1 {
	public static boolean deleteFile(String path) {
		File dir = new File(path);
		if (dir.exists()) {
			return dir.delete();
		}

		return false;
	}

	public static boolean deleteFolder(String path) {
		File file = new File(path);

		if (file.exists()) {
			if (file.isFile()) {
				return deleteFile(path);
			} else {
				String[] list = file.list();
				for (int i = 0; i < list.length; i++) {
					deleteFolder(path + "\\" + list[i]);
				}
				return file.delete();
			}
		} else {
			return false;
		}
	}
}
