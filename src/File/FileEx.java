package File;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class FileEx {

	public static boolean deleteFile(String path) {
		File dir = new File(path);
		if (dir.exists()) {
			return dir.delete();
		}

		return false;
	}

	public static boolean deleteFolder(String path) {
		File file = new File(path);

		if (file.exists()) {
			if (file.isFile()) {
				return deleteFile(path);
			} else {
				String[] list = file.list();
				for (int i = 0; i < list.length; i++) {
					deleteFolder(path + "\\" + list[i]);
				}
				return file.delete();
			}
		} else {
			return false;
		}
	}

	public static boolean findFirst(String path, String pattern) {
		File dir = new File(path);

		File[] list = dir.listFiles();

		boolean result = false;

		for (int i = 0; i < list.length; i++) {

			if (list[i].getName().equalsIgnoreCase(pattern)) {

				System.out.println(list[i]);

				result = true;

			}

		}

		return result;
	}

	public static void RecursivePrint(File[] arr, int index, int level) {
		// terminate condition
		if (index == arr.length)
			return;

		// tabs for internal levels
		for (int i = 0; i < level; i++)
			System.out.print("--");

		// for files
		if (arr[index].isFile())
			System.out.println("|+" + arr[index].getName());

		// for sub-directories
		else if (arr[index].isDirectory()) {

			System.out.println("+" + arr[index].getName());
			
			// recursion for sub-directories
			RecursivePrint(arr[index].listFiles(), 0, level + 3);

		}

		// recursion for main directory
		RecursivePrint(arr, ++index, level);
	}

	public static void dirTree(String path) {
		File dir = new File(path);

		File[] fList = dir.listFiles();

		RecursivePrint(fList, 0, 0);

	}

	public static void findAll(String path, String ext) {
		File dir = new File(path);

		File[] fList = dir.listFiles();

		for (int i = 0; i < fList.length; i++) {
			if (fList[i].isFile()) {
				if (fList[i].getName().endsWith(ext)) {
					System.out.println(fList[i].getPath());
				}

			} else {
				findAll(fList[i].getAbsolutePath(), ext);
			}
		}
	}

	public static void findAlls(String path, String[] exts) {
		for (int i = 0; i < exts.length; i++) {
			findAll(path, exts[i]);
		}
	}

	public static void deleteAll(String path, String ext) {
		File dir = new File(path);

		File[] fList = dir.listFiles();

		for (int i = 0; i < fList.length; i++) {
			if (fList[i].isFile()) {
				if (fList[i].getName().endsWith(ext)) {
					fList[i].delete();
				}

			} else {
				deleteAll(fList[i].getAbsolutePath(), ext);
			}
		}
	}

	public static void deleteAlls(String path, String[] exts) {
		File dir = new File(path);

		File[] fList = dir.listFiles();

		for (int i = 0; i < fList.length; i++) {
			if (fList[i].isFile()) {
				for (int j = 0; j < exts.length; j++) {
					if (fList[i].getName().endsWith(exts[j])) {
						System.out.println(fList[i].delete());
					}

				}

			} else {
				deleteAlls(fList[i].getAbsolutePath(), exts);
			}
		}
	}

	public static void copyAll(String sDir, String dDir, String ext) throws IOException {
		File sPath = new File(sDir);

		File dPath = new File(dDir);

		File[] sListFile = sPath.listFiles();
		for (int i = 0; i < sListFile.length; i++) {
			if (sListFile[i].isFile() && sListFile[i].getName().endsWith(ext)) {
				Files.copy(sListFile[i].toPath(), new File(dPath + "\\" + sListFile[i].getName()).toPath(),
						StandardCopyOption.REPLACE_EXISTING);
			}
		}

	}

	public static void main(String[] args) throws IOException {
		String path = "D:\\Test";
//		String pattern = "Movies";
//		System.out.println(findFirst(path, pattern));

//		dirTree(path);

//		String p1 = "D:\\Test";
//		String p2 = "D:\\Test\\f2";
//		copyAll(p1, p2, ".txt");
		
		dirTree(path);

	}

}
