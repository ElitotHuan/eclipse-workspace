package File;

import java.io.File;

public class Bai6 {
	public static void deleteAll(String path, String ext) {
		File dir = new File(path);

		File[] fList = dir.listFiles();

		for (int i = 0; i < fList.length; i++) {
			if (fList[i].isFile()) {
				if (fList[i].getName().endsWith(ext)) {
					fList[i].delete();
				}

			} else {
				deleteAll(fList[i].getAbsolutePath(), ext);
			}
		}
	}

	public static void deleteAlls(String path, String[] exts) {
		File dir = new File(path);

		File[] fList = dir.listFiles();

		for (int i = 0; i < fList.length; i++) {
			if (fList[i].isFile()) {
				for (int j = 0; j < exts.length; j++) {
					if (fList[i].getName().endsWith(exts[j])) {
						System.out.println(fList[i].delete());
					}

				}

			} else {
				deleteAlls(fList[i].getAbsolutePath(), exts);
			}
		}
	}
}
