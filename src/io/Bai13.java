package io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Bai13 {

	public static void saveStudents(List<SinhVien> ls, String pathFile) throws IOException {
		File s = new File(pathFile);

		if (!s.exists() || !s.isFile()) {
			return;
		}

		RandomAccessFile raf = new RandomAccessFile(s, "rw");
		raf.writeInt(ls.size());

		for (int i = 0; i < ls.size(); i++) {
			raf.writeLong(0L);
		}

		long pos;
		for (int i = 0; i < ls.size(); i++) {
			pos = raf.getFilePointer();
			raf.seek(4L + i * 8);
			raf.writeLong(pos);
			raf.seek(pos);
			ls.get(i).save(raf);
		}

	}

	public static SinhVien load(String path, int index) throws IOException {
		File sFile = new File(path);
		if (!sFile.exists() || !sFile.isFile())
			return null;

		RandomAccessFile raf = new RandomAccessFile(sFile, "rw");
		int size = raf.readInt();
		if (index < 1 || index > size) {
			raf.close();
			return null;
		}

		raf.seek(4L + (index - 1) * 8);
		long pos = raf.readLong();
		raf.seek(pos);

		SinhVien sv = new SinhVien();
		sv.load(raf);

		raf.close();
		return sv;
	}

	public static void updateSV(String path, int index, String maSV, String name, int age, double diem)
			throws IOException {
		File sFile = new File(path);
		if (!sFile.exists() || !sFile.isFile())
			return;

		RandomAccessFile raf = new RandomAccessFile(sFile, "rw");
		int size = raf.readInt();
		if (index < 1 || index > size) {
			raf.close();
			return;
		}

		// lấy length hiện tại của file
		long length = raf.length();
		// lấy tất cả các pointer
		List<Long> pointers = new ArrayList<Long>();
		for (int i = 0; i < size; i++)
			pointers.add(raf.readLong());
		// lấy pointer của sinh viên bị update name
		long p = pointers.get(index - 1);

		// lưu các sinh viên phía sau "sinh viên bị update name"
		Queue<SinhVien> queue = new LinkedList<SinhVien>();
		SinhVien sv;
		for (int i = 0; i < size - index; i++) {
			sv = new SinhVien();
			sv.load(raf);
			queue.offer(sv);
		}
		
		
		// update name cho sinh viên
		raf.seek(p);
		raf.writeUTF(maSV);
		raf.writeUTF(name);
		raf.writeInt(age);
		raf.writeDouble(diem);

		/*
		 * Sau khi update name, nếu có sự chênh lệch số bytes giữa "new name" và
		 * "old name" => Phải update lại pointer của các sinh viên phía sau
		 * 
		 * Ví dụ name là "Vàng", update thành "Lê Viết Nhả" => số bytes "new name" > số
		 * bytes "old name" => pointer của các sinh viên phía sau phải tăng lên
		 * 
		 * Ngược lại nếu số bytes "new name" < "old name" Thì pointer của các sinh viên
		 * phía sau phải giảm xuống
		 *
		 */

		long d = (index == size) ? raf.getFilePointer() - length : raf.getFilePointer() - pointers.get(index);
		if (d != 0)
			for (int i = index; i < size; i++)
				pointers.set(i, pointers.get(i) + d);

		// write lại các sinh viên phía sau
		while (!queue.isEmpty()) {
			sv = queue.poll();
			sv.save(raf);
		}

		// update lại pointer của các sinh viên phía sau
		if (d != 0) {
			raf.seek(4L + index * 8);
			for (int i = index; i < size; i++)
				raf.writeLong(pointers.get(i));
		}

		// chỗ này nếu số bytes "new name" < "old name"
		// => phải setLength lại để: length sau khi update phải nhỏ hơn length ban đầu
		// nếu không setLength lại thì vẫn đọc được chính xác sinh viên
		// nhưng file sẽ bị dư một ít bytes ở cuối file (dùng HxD để thấy rõ hơn)
		if (d < 0)
			raf.setLength(length + d);

		raf.close();

	}

	public static void main(String[] args) throws IOException {
		SinhVien sv1 = new SinhVien("1", "Hoa", 18, 5.0);
		SinhVien sv2 = new SinhVien("2", "Hồng", 19, 8.0);
		SinhVien sv3 = new SinhVien("3", "Tú", 17, 5.5);

		List<SinhVien> ls = new ArrayList<>();
		ls.add(sv1);
		ls.add(sv2);
		ls.add(sv3);

//		saveStudents(ls, "D:\\Test\\save.txt");

		updateSV("D:\\Test\\save.txt", 1, "4", "Võ Đoàn Minh Huân", 20, 10.0);

		SinhVien v = load("D:\\Test\\save.txt", 1);
		System.out.println(v);

	}
}
