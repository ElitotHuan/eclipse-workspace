package io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class IOEx {

	public static boolean fileCopy(String sFile, String dFile, boolean moved) throws IOException {
		File source = new File(sFile);
		File dest = new File(dFile);

		if (!source.exists()) {
			return false;
		} else {
			InputStream fis = new BufferedInputStream(new FileInputStream(source));
			OutputStream fos = new BufferedOutputStream(new FileOutputStream(dest));

			byte[] input = new byte[1024];
			int size = (int) source.length();
			while (fis.read(input) != -1) {
				fos.write(input, 0, size);

			}

			fos.flush();
			fis.close();
			fos.close();

			if (moved) {
				return source.delete();

			}

		}

		return false;

	}

	public static boolean folderCopy(String sFolder, String dFolder, boolean moved) throws IOException {
		File source = new File(sFolder);

		File dest = new File(dFolder);

		boolean check = false;

		if (source.exists()) {
			if (source.isFile()) {
				check = fileCopy(sFolder, dFolder + "\\" + source.getName(), true);
			} else {
				if (!dest.exists())
					dest.mkdir();

				String[] list = source.list();
				for (int i = 0; i < list.length; i++) {
					check = folderCopy(sFolder + "\\" + list[i], dFolder, true);
				}

				source.delete();
			}
		}

		return check;
	}

	public static boolean splitFile(String source, long size) throws IOException {
		File s = new File(source);
		if (s.exists()) {
			InputStream bis = new BufferedInputStream(new FileInputStream(source));
			long demFile = s.length() / size;
			long byteDu = s.length() % size;
			for (int i = 0; i < demFile; i++) {
				String dFile = s.getAbsolutePath() + "0" + i;
				OutputStream bos = new BufferedOutputStream(new FileOutputStream(dFile));
				writeFile(size, bis, bos);
				bos.flush();
				bos.close();
			}

			if (byteDu > 0) {
				String dFile = s.getAbsolutePath() + "0x";
				OutputStream bos = new BufferedOutputStream(new FileOutputStream(dFile));
				writeFile(byteDu, bis, bos);
				bos.flush();
				bos.close();
			}

			return true;
		} else {
			return false;
		}

	}

	public static boolean joinFile(String source) throws IOException {
		File s = new File(source);
		if (!s.exists()) {
			return false;
		} else {
			String extension = source.substring((int) s.length() - 4, (int) s.length() - 1);
			File folder = new File(s.getParent());
			File[] dsFile = folder.listFiles();
			OutputStream bos = new BufferedOutputStream(new FileOutputStream(folder.getAbsoluteFile()));
			System.out.println(folder.getAbsolutePath() + "\\" + "Joiner" + extension);
			for (File file : dsFile) {
				String name = file.getName().substring(0, file.getName().length() - 2);
				if (name.equals(s.getName())) {
					InputStream bis = new BufferedInputStream(new FileInputStream(file));
					byte[] byteRead = new byte[1024 * 1000];
					int i;
					while ((i = bis.read(byteRead)) != -1) {
						bos.write(byteRead, 0, i);

					}
				}
			}

			return true;

		}
	}

	public static void writeFile(long size, InputStream bis, OutputStream bos) throws IOException {
		for (int i = 0; i < size; i++) {
			bos.write(bis.read());
		}
	}

	public static void save(ArrayList<SinhVien> dssv, String path) throws IOException {
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(path));
		out.writeInt(dssv.size());
		for (SinhVien sv : dssv) {
			out.writeUTF(sv.getMasV());
			out.writeUTF(sv.getHoTen());
			out.writeInt(sv.getTuoi());
			out.writeDouble(sv.getDiemTB());
			out.flush();
		}

	}

	public static ArrayList<SinhVien> load(String path) throws IOException {
		ArrayList<SinhVien> dssv = new ArrayList<SinhVien>();
		FileInputStream fis = new FileInputStream(path);
		ObjectInputStream ois = new ObjectInputStream(fis);
		int size = ois.readInt();

		for (int i = 0; i < size; i++) {
//
//			SinhVien sv = new SinhVien(ois.readUTF(), ois.readUTF(), ois., ois.readInt(), ois.readDouble());
//			dssv.add(sv);
		}

		return dssv;

	}

//	public static void saveByRAF(ArrayList<SinhVien> dssv, String path) throws IOException {
//		RandomAccessFile raf = new RandomAccessFile(new File(path), "rw");
//		raf.writeInt(dssv.size());
//		for (SinhVien sv : dssv) {
//			raf.writeUTF(sv.getMasV());
//			raf.writeUTF(sv.getHoTen());
//			raf.writeInt(sv.getTuoi());
//			raf.writeDouble(sv.getDiemTB());
//		}
//
//	}

//	public static SinhVien loadByRAF(String path, int index) {
//		
//	}

	public static void main(String[] args) throws IOException {
//		String sFile = "D:\\Test\\test3.txt";
//		String dFile = "D:\\Test\\f1\\f3.txt";
//
//		String sFolder = "D:\\Test\\f1";
//		String dFolder = "D:\\Test\\f2";
//
//		String fs = "D:\\Test\\ex.docx";

//		File s = new File(sFile);
//		File d = new File(dFile);
//		System.out.println(s.equals(d));

//		String save = "D:\\Test\\student.txt";
//		ArrayList<SinhVien> dssv = new ArrayList<>();
//		SinhVien sv1 = new SinhVien("1", "Hoa", new Date(1, 12, 1999), 18, 5.0);
//		SinhVien sv2 = new SinhVien("2", "Hồng", new Date(31, 1, 1994), 19, 8.0);
//		SinhVien sv3 = new SinhVien("3", "Tú", new Date(23, 2, 1996), 17, 5.5);
//
//		dssv.add(sv1);
//		dssv.add(sv2);
//		dssv.add(sv3);

//		save(dssv, save);

//		ArrayList<SinhVien> t = load(save);
//		for (SinhVien sinhVien : t) {
//			System.out.println(sinhVien.toString());
//		}

	}
}
