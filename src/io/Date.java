package io;

public class Date {
	private int day, month, year;

	public Date(int day, int month, int year) {
		// TODO Auto-generated constructor stub
		this.day = day;
		this.month = month;
		this.year = year;
	}

	@Override
	public String toString() {
		return "Date [day=" + day + ", month=" + month + ", year=" + year + "]";
	}

}
