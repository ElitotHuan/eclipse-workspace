package io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;

public class SinhVien implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String masV, hoTen;
	private int tuoi;
	private double diemTB;
	
	public SinhVien() {
		
	}
	
	public SinhVien(String masV, String hoTen, int tuoi, double diemTB) {
		super();
		this.masV = masV;
		this.hoTen = hoTen;

		this.tuoi = tuoi;
		this.diemTB = diemTB;
	}

	@Override
	public String toString() {
		return "SinhVien [masV=" + masV + ", hoTen=" + hoTen + ", " + " tuoi=" + tuoi + ", diemTB=" + diemTB + "]";
	}

	public String getMasV() {
		return masV;
	}

	public String getHoTen() {
		return hoTen;
	}

	public int getTuoi() {
		return tuoi;
	}

	public double getDiemTB() {
		return diemTB;
	}

	public void save(DataOutput stream) throws IOException {
		stream.writeUTF(masV);
		stream.writeUTF(hoTen);
		stream.writeInt(tuoi);
		stream.writeDouble(diemTB);
	}

	public void load(DataInput stream) throws IOException {
		masV = stream.readUTF();
		hoTen = stream.readUTF();
		tuoi = stream.readInt();
		diemTB = stream.readDouble();
	}

}
